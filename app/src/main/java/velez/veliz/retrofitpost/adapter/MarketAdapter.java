package velez.veliz.retrofitpost.adapter;

import java.util.List;

import retrofit2.Call;
import velez.veliz.retrofitpost.constants.ApiConstants;
import velez.veliz.retrofitpost.model.Post;
import velez.veliz.retrofitpost.service.MarketService;

public class MarketAdapter extends  BaseAdapter implements MarketService{


     private MarketService marketService;

     public MarketAdapter(){
       super(ApiConstants.BASE_POST_URL);
       marketService = createService(MarketService.class);
     }

      @Override
      public  Call<Post> InsertPost (Post post) {
          return marketService.InsertPost(post);
      }

    @Override
    public Call<List<Post>> getPosts() {
        return null;
    }


}


