package velez.veliz.retrofitpost;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import velez.veliz.retrofitpost.adapter.MarketAdapter;
import velez.veliz.retrofitpost.model.Post;

public class MainActivity extends AppCompatActivity {

    EditText title,description,urlImage;
    Button enviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        title=findViewById(R.id.title);
        description=findViewById(R.id.description);
        urlImage=findViewById(R.id.urlImage);
        enviar=findViewById(R.id.enviar);

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostPosts();
            }
        });

    }
    private void PostPosts(){
        MarketAdapter adapter =new MarketAdapter();
        retrofit2.Call<Post> call=adapter.InsertPost(
                new Post(
                        title.getText().toString(),
                        description.getText().toString()
                        ,"https://img.freepik.com/foto-gratis/playa-phuket-tailandia_38810-691.jpg?size=626&ext=jpg"
                ));

        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(retrofit2.Call<Post> call, Response<Post> response) {

            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {

            }
        });
    }
}