package velez.veliz.retrofitpost.constants;

public class ApiConstants {

    //BASE URL
            //https://backend-posts.herokuapp.com/

    public static  final String BASE_POST_URL = "https://backend-posts.herokuapp.com/";


    public static final  String MARKET_POST_ENDPOINT = "posts/";
}
