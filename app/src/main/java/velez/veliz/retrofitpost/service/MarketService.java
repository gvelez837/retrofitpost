package velez.veliz.retrofitpost.service;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import velez.veliz.retrofitpost.constants.ApiConstants;
import velez.veliz.retrofitpost.model.Post;

public interface MarketService {
    @POST(ApiConstants.MARKET_POST_ENDPOINT)
    Call<Post> InsertPost (@Body Post post);
    @GET(ApiConstants.MARKET_POST_ENDPOINT)
    Call<List<Post>> getPosts();
}